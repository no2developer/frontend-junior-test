Hola futuro colega **¡Felicitaciones llegaste a la prueba final! **

Ahora solo falta demostrarnos tu talento haciendo lo mejor que saber hacer: Programar ñ_ñ

En la carpeta **assets** encontrarás el problema que debes resolver, **toda tu solución
debe ajustarse a la estructura de carpetas ya establecidas** dentro del folder Frontend.

Entonces, que esperas? **Haz un fork** y empieza a subir tu código.

Exitos!